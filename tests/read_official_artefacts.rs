extern crate env_logger;
extern crate rust_release_artefact as rra;
extern crate tempfile;

use std::fs;
use std::io;

fn assert_artefact_content(artefact: rra::ExtractedArtefact) {
    assert_eq!(artefact.version, "0.3.4 (6714a447d 2017-12-23)");

    assert_eq!(artefact.git_commit_hash, None);

    assert_eq!(
        artefact.components.keys().collect::<Vec<_>>(),
        ["rustfmt-preview"],
    );

    let rustfmt = artefact
        .components
        .get("rustfmt-preview")
        .expect("Missing component??");

    let files = rustfmt
        .files
        .iter()
        .map(|path| path.to_str().expect("Invalid path in artefact?"))
        .collect::<Vec<_>>();
    assert_eq!(
        files,
        [
            "bin/cargo-fmt",
            "bin/rustfmt",
            "share/doc/rustfmt/LICENSE-APACHE",
            "share/doc/rustfmt/LICENSE-MIT",
            "share/doc/rustfmt/README.md"
        ],
    );
}

#[test]
fn open_tar_gz() {
    let _ = env_logger::try_init();

    let input = fs::File::open(
        "tests/rustfmt-0.3.4-x86_64-unknown-linux-gnu.tar.gz",
    ).expect("could not open artefact?");

    let stage = tempfile::tempdir().expect("Could not create temp dir?");
    let artefact = rra::ExtractedArtefact::from_tar_gz(
        io::BufReader::new(input),
        stage.path(),
    ).expect("Could not unpack artefact?");

    assert_artefact_content(artefact);
}

#[test]
fn open_tar_xz() {
    let _ = env_logger::try_init();

    let input = fs::File::open(
        "tests/rustfmt-0.3.4-x86_64-unknown-linux-gnu.tar.xz",
    ).expect("could not open artefact?");

    let stage = tempfile::tempdir().expect("Could not create temp dir?");
    let artefact = rra::ExtractedArtefact::from_tar_xz(
        io::BufReader::new(input),
        stage.path(),
    ).expect("Could not unpack artefact?");

    assert_artefact_content(artefact);
}

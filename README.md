rust_release_artefact: Extract and install Rust release artefacts
=================================================================

Repository: <https://gitlab.com/Screwtapello/rust_release_artefact>

Documentation: <https://docs.rs/rust_release_artefact>

TODO
----

  - Update `ExtractedArtefact::new()` to use `walkdir` to search for artefact
    marker files.
  - Would it be neater to toss the first path-component of archives, rather than
    extracting it and having to rummage around to find the artefact metadata?

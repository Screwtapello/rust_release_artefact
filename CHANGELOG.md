All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog]
and this project adheres to [Semantic Versioning].

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]
============

Nothing.

[0.1.3] - 2018-05-08
====================

Changed
-------

   - Further testing on Windows revealed that additional internal changes
     were necessary to reliably work with artefacts on Windows. A new example
     (`install-component`) was added to assist with the testing process.

[0.1.2] - 2018-05-08
====================

Added
-----

  - Started keeping a change log.

Changed
-------

  - Testing on Windows revealed that Rust release artefacts may contain more
    deeply-nested paths than fit in Windows' standard 260-character path limit.
    Updated the documentation to recommend callers use `std::fs::canonicalize()`
    on paths before passing them to this crate, since the Windows canonical
    form lifts the path limit to about 32,000 characters.

[0.1.1] - 2018-05-01
====================

Changed
-------

  - The `Component::install_to()` method now tries to remove an existing file
    from the destination directory before hard-linking or copying its
    replacement into place, where previously an existing file would make it give
    up. This *should* be just as safe, but makes installation behave more like
    just copying files into place as people expect.


[0.1.0] - 2018-04-19
====================

Initial release.

[Unreleased]: https://gitlab.com/Screwtapello/rustbud/compare/v0.1.3...master
[0.1.3]: https://gitlab.com/Screwtapello/rustbud/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/Screwtapello/rustbud/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/Screwtapello/rustbud/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/Screwtapello/rust_release_artefact/tree/v0.1.0
